package org.minbox.chapter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 加载配置类
 *
 * @author 恒宇少年
 */
@Configuration
public class LoadConfig {
    /**
     * 配置读取name属性，不存在时使用空字符为默认值
     */
    @Value("${name:''}")
    private String name;

    public String getName() {
        return name;
    }
}
